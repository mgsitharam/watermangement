package com.app.watermangement.httpClient;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class URL {
    private static String BASE_URL = "http://192.168.4.1";

    public String getBaseURL(Activity activity) {
        SharedPreferences sharedpreferences = activity.getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        boolean isKey = sharedpreferences.contains("ipAddress");
        String url = "";
        if (isKey) {
            url = sharedpreferences.getString("ipAddress", "http://192.168.4.1");
            Log.d("ipAddress2", sharedpreferences.getString("ipAddress", "http://192.168.4.1"));
        } else {
            url = "http://192.168.4.1";
        }
        return url;
    }

    public static final String SYS_INFO = "/sysinfo";
    public static final String GET_CONFIG = "/getconfig";
    public static final String MOTOR_ON = "/motoron";
    public static final String MOTOR_OFF = "/motoroff";
    public static final String MOTOR_LEVEL = "/waterlevel";
    public static final String MOTOR_STATE = "/motorstate";
}