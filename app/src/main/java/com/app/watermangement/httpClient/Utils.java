package com.app.watermangement.httpClient;

import android.app.ProgressDialog;
import android.content.Context;

public class Utils {

    static ProgressDialog dialog;

    public static void startLoadingScreen(Context mContext) {
        if (dialog == null) {
            dialog = new ProgressDialog(mContext);
            dialog.setMessage("Loading");
            dialog.setCancelable(false);
            dialog.setInverseBackgroundForced(false);
            dialog.show();
        }

    }

    public static void cancelLoadingScreen() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
