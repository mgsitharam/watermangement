package com.app.watermangement.httpClient;

public class Constants {
    public static final int REQUEST_SYS_INFO = 1000;
    public static final int REQUEST_GET_CONFIG = 1001;
    public static final int REQUEST_MOTOR_ON = 1002;
    public static final int REQUEST_MOTOR_OFF = 1003;
    public static final int REQUEST_MOTOR_LEVEL = 1004;
    public static final int REQUEST_MOTOR_STATE = 1005;
}