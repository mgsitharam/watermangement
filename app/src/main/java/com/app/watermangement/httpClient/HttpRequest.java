package com.app.watermangement.httpClient;


import android.app.Activity;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.watermangement.RequestInterface;
import com.app.watermangement.ResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class HttpRequest implements RequestInterface {
    private ResponseHandler mCallBack;


    @Override
    public void request(Activity mActivity, JSONObject requestObject, final int requestNumber, String url) {
        mCallBack = (ResponseHandler) mActivity;
        Log.d("url", url);

        RequestQueue queue = Volley.newRequestQueue(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("Motor State", "" + response);
                            JSONObject responseData = new JSONObject(response);
                            Log.d("Motor State", "" + responseData.toString());
                            mCallBack.responseHandler(responseData, requestNumber);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error---->", error.toString());
                mCallBack.responseHandler(null, requestNumber);
            }
        });
        queue.add(stringRequest);
    }

}
