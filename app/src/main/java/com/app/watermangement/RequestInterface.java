package com.app.watermangement;


import android.app.Activity;

import org.json.JSONObject;

public interface RequestInterface {
    void request(Activity mActivity, JSONObject requestObject, int requestNumber, String url);
}
