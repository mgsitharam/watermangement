package com.app.watermangement;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.watermangement.httpClient.Constants;
import com.app.watermangement.httpClient.HttpRequest;
import com.app.watermangement.httpClient.URL;
import com.app.watermangement.httpClient.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements ResponseHandler {

    private HttpRequest httpRequest;
    private LinearLayout motorStatusLayout, waterLevelLayout, layoutMotorDetails, sysInfoLayout, getConfigLayout;
    private TextView model, serialNo, firmWareVersion, buildNo, ipAddress, macAddress, deviceUpTime, maximumMotorDuration,
            operationMode, dryRunProtectionEnabled, waitBeforeAutoRestart;
    private TextView resultTxt, resultStrTxt, water_over_head, sump_water, motor_state, reason, prev_reason, prev_runtime, motor_stop_at;
    private Button motorOn, motorOff, motorLevelBtn, detailsBtn, sysInfoBtn, getconfigBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        httpRequest = new HttpRequest();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setIcon(R.drawable.actionbar_space_between_icon_and_title);
        }
        initViews();
        listeners();
    }

    private void initViews() {
        sysInfoBtn = findViewById(R.id.sysInfoBtn);
        getconfigBtn = findViewById(R.id.getconfigBtn);
        motorOn = findViewById(R.id.motorOn);
        motorOff = findViewById(R.id.motorOff);
        motorStatusLayout = findViewById(R.id.motorStatusLayout);
        waterLevelLayout = findViewById(R.id.water_level_layout);
        layoutMotorDetails = findViewById(R.id.layout_motor_details);
        sysInfoLayout = findViewById(R.id.sysInfoLayout);
        getConfigLayout = findViewById(R.id.getConfigLayout);
        resultTxt = findViewById(R.id.resultTxt);
        resultStrTxt = findViewById(R.id.resultStrTxt);
        model = findViewById(R.id.model);
        serialNo = findViewById(R.id.serialNo);
        firmWareVersion = findViewById(R.id.firmWareVersion);
        buildNo = findViewById(R.id.buildNo);
        ipAddress = findViewById(R.id.ipAddress);
        macAddress = findViewById(R.id.macAddress);
        deviceUpTime = findViewById(R.id.deviceUpTime);
        maximumMotorDuration = findViewById(R.id.maximumMotorDuration);
        operationMode = findViewById(R.id.operationMode);
        dryRunProtectionEnabled = findViewById(R.id.dryRunProtectionEnabled);
        waitBeforeAutoRestart = findViewById(R.id.waitBeforeAutoRestart);
        water_over_head = findViewById(R.id.water_over_head);
        sump_water = findViewById(R.id.sump_water);
        motor_state = findViewById(R.id.motor_state);
        reason = findViewById(R.id.reason);
        prev_reason = findViewById(R.id.prev_reason);
        prev_runtime = findViewById(R.id.prev_runtime);
        motor_stop_at = findViewById(R.id.motor_stop_at);
        detailsBtn = findViewById(R.id.details);
        motorLevelBtn = findViewById(R.id.motorlevel);
    }

    private void listeners() {

        sysInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysInfoBtn.setBackgroundColor(Color.parseColor("#2C72FF"));
                getconfigBtn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOff.setBackgroundColor(Color.parseColor("#172B51"));
                motorLevelBtn.setBackgroundColor(Color.parseColor("#172B51"));
                detailsBtn.setBackgroundColor(Color.parseColor("#172B51"));
                Utils.startLoadingScreen(MainActivity.this);
                httpRequest.request(MainActivity.this, null, Constants.REQUEST_SYS_INFO,
                        new URL().getBaseURL(MainActivity.this) + URL.SYS_INFO)
                ;
            }
        });

        getconfigBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysInfoBtn.setBackgroundColor(Color.parseColor("#172B51"));
                getconfigBtn.setBackgroundColor(Color.parseColor("#2C72FF"));
                motorOn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOff.setBackgroundColor(Color.parseColor("#172B51"));
                motorLevelBtn.setBackgroundColor(Color.parseColor("#172B51"));
                detailsBtn.setBackgroundColor(Color.parseColor("#172B51"));
                Utils.startLoadingScreen(MainActivity.this);
                httpRequest.request(MainActivity.this, null, Constants.REQUEST_GET_CONFIG, new URL().getBaseURL(MainActivity.this) + URL.GET_CONFIG);
            }
        });

        motorOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysInfoBtn.setBackgroundColor(Color.parseColor("#172B51"));
                getconfigBtn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOn.setBackgroundColor(Color.parseColor("#2C72FF"));
                motorOff.setBackgroundColor(Color.parseColor("#172B51"));
                motorLevelBtn.setBackgroundColor(Color.parseColor("#172B51"));
                detailsBtn.setBackgroundColor(Color.parseColor("#172B51"));
                Utils.startLoadingScreen(MainActivity.this);
                httpRequest.request(MainActivity.this, null, Constants.REQUEST_MOTOR_ON, new URL().getBaseURL(MainActivity.this) + URL.MOTOR_ON);
            }
        });
        motorOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysInfoBtn.setBackgroundColor(Color.parseColor("#172B51"));
                getconfigBtn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOff.setBackgroundColor(Color.parseColor("#2C72FF"));
                motorOn.setBackgroundColor(Color.parseColor("#172B51"));
                motorLevelBtn.setBackgroundColor(Color.parseColor("#172B51"));
                detailsBtn.setBackgroundColor(Color.parseColor("#172B51"));
                Utils.startLoadingScreen(MainActivity.this);
                httpRequest.request(MainActivity.this, null, Constants.REQUEST_MOTOR_OFF, new URL().getBaseURL(MainActivity.this) + URL.MOTOR_OFF);
            }
        });
        motorLevelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysInfoBtn.setBackgroundColor(Color.parseColor("#172B51"));
                getconfigBtn.setBackgroundColor(Color.parseColor("#172B51"));
                motorLevelBtn.setBackgroundColor(Color.parseColor("#2C72FF"));
                motorOn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOff.setBackgroundColor(Color.parseColor("#172B51"));
                detailsBtn.setBackgroundColor(Color.parseColor("#172B51"));
                Utils.startLoadingScreen(MainActivity.this);
                httpRequest.request(MainActivity.this, null, Constants.REQUEST_MOTOR_LEVEL, new URL().getBaseURL(MainActivity.this) + URL.MOTOR_LEVEL);
            }
        });
        detailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sysInfoBtn.setBackgroundColor(Color.parseColor("#172B51"));
                getconfigBtn.setBackgroundColor(Color.parseColor("#172B51"));
                detailsBtn.setBackgroundColor(Color.parseColor("#2C72FF"));
                motorOn.setBackgroundColor(Color.parseColor("#172B51"));
                motorOff.setBackgroundColor(Color.parseColor("#172B51"));
                motorLevelBtn.setBackgroundColor(Color.parseColor("#172B51"));
                Utils.startLoadingScreen(MainActivity.this);
                httpRequest.request(MainActivity.this, null, Constants.REQUEST_MOTOR_STATE, new URL().getBaseURL(MainActivity.this) + URL.MOTOR_STATE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.main_activity_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ipAddress:
                openDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter IP");
        LinearLayout layout = new LinearLayout(MainActivity.this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(15, 5, 5, 5);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
        layout.setPadding(10, 10, 5, 5);

// Set up the input
        final EditText input = new EditText(this);
        input.setId(R.id.ipAddress);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        layout.addView(input);

        TextView textView = new TextView(this);
        textView.setText("Ex:http://192.168.4.1");
        textView.setPadding(10, 0, 0, 0);
        textView.setTextColor(Color.parseColor("#A9A9A9"));
        layout.addView(textView);
        builder.setView(layout);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                SharedPreferences sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("ipAddress", m_Text);
                editor.apply();
                Log.d("ipAddress1", m_Text);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public void responseHandler(Object response, int requestType) {
        switch (requestType) {
            case Constants.REQUEST_MOTOR_ON:
            case Constants.REQUEST_MOTOR_OFF:
                Utils.cancelLoadingScreen();
                if (response != null) {
                    try {
                        JSONObject locObj = (JSONObject) response;
                        if (locObj.getString("result").equals("Success")) {
                            Log.d("locObj", locObj.toString());
                            motorStatusLayout.setVisibility(View.VISIBLE);
                            layoutMotorDetails.setVisibility(View.GONE);
                            waterLevelLayout.setVisibility(View.GONE);
                            sysInfoLayout.setVisibility(View.GONE);
                            getConfigLayout.setVisibility(View.GONE);
                            resultTxt.setText(locObj.getString("result"));
                            resultStrTxt.setText(locObj.getString("result_str"));
                            Toast.makeText(this, locObj.getString("result_str"), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    motorStatusLayout.setVisibility(View.GONE);
                    layoutMotorDetails.setVisibility(View.GONE);
                    waterLevelLayout.setVisibility(View.GONE);
                    Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                }
                break;

            case Constants.REQUEST_MOTOR_LEVEL:
                Utils.cancelLoadingScreen();
                if (response != null) {
                    try {
                        JSONObject locObj = (JSONObject) response;
                        if (locObj.getString("result").equals("Success")) {
                            Log.d("locObj", locObj.toString());
                            motorStatusLayout.setVisibility(View.VISIBLE);
                            layoutMotorDetails.setVisibility(View.GONE);
                            waterLevelLayout.setVisibility(View.VISIBLE);
                            sysInfoLayout.setVisibility(View.GONE);
                            getConfigLayout.setVisibility(View.GONE);
                            resultTxt.setText(locObj.getString("result"));
                            water_over_head.setText(locObj.getJSONObject("result_str").getString("Overhead Tank Water Level"));
                            sump_water.setText(locObj.getJSONObject("result_str").getString("Sump Water Level"));
                        } else {
                            Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    motorStatusLayout.setVisibility(View.GONE);
                    layoutMotorDetails.setVisibility(View.GONE);
                    waterLevelLayout.setVisibility(View.GONE);
                    Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                }
                break;

            case Constants.REQUEST_MOTOR_STATE:
                Utils.cancelLoadingScreen();
                if (response != null) {
                    try {
                        JSONObject locObj = (JSONObject) response;
                        if (locObj.getString("result").equals("Success")) {
                            Log.d("locObj", locObj.toString());
                            motorStatusLayout.setVisibility(View.GONE);
                            layoutMotorDetails.setVisibility(View.VISIBLE);
                            waterLevelLayout.setVisibility(View.GONE);
                            sysInfoLayout.setVisibility(View.GONE);
                            getConfigLayout.setVisibility(View.GONE);
                            resultTxt.setText(locObj.getString("result"));
                            motor_state.setText(locObj.getJSONObject("result_str").getString("Motor State"));
                            reason.setText(locObj.getJSONObject("result_str").getString("Reason"));
                            prev_reason.setText(locObj.getJSONObject("result_str").getString("Previous Start Reason"));
                            prev_runtime.setText(locObj.getJSONObject("result_str").getString("Previous Run Time"));
                            motor_stop_at.setText(locObj.getJSONObject("result_str").getString("Motor Stopped At"));
                        } else {
                            Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    motorStatusLayout.setVisibility(View.GONE);
                    layoutMotorDetails.setVisibility(View.GONE);
                    waterLevelLayout.setVisibility(View.GONE);
                    Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                }
                break;

            case Constants.REQUEST_SYS_INFO:
                Utils.cancelLoadingScreen();
                if (response != null) {
                    try {
                        JSONObject locObj = (JSONObject) response;
                        if (locObj.getString("result").equals("Success")) {
                            Log.d("locObj", locObj.toString());
                            motorStatusLayout.setVisibility(View.GONE);
                            layoutMotorDetails.setVisibility(View.GONE);
                            waterLevelLayout.setVisibility(View.GONE);
                            sysInfoLayout.setVisibility(View.VISIBLE);
                            getConfigLayout.setVisibility(View.GONE);
                            resultTxt.setText(locObj.getString("result"));
                            model.setText(locObj.getJSONObject("result_str").getString("Model"));
                            serialNo.setText(locObj.getJSONObject("result_str").getString("Serial No"));
                            firmWareVersion.setText(locObj.getJSONObject("result_str").getString("Firmware Version"));
                            buildNo.setText(locObj.getJSONObject("result_str").getString("Build No"));
                            ipAddress.setText(locObj.getJSONObject("result_str").getString("IP Address"));
                            macAddress.setText(locObj.getJSONObject("result_str").getString("MAC Address"));
                            deviceUpTime.setText(locObj.getJSONObject("result_str").getString("Device Up Time"));
                        } else {
                            Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    motorStatusLayout.setVisibility(View.GONE);
                    layoutMotorDetails.setVisibility(View.GONE);
                    waterLevelLayout.setVisibility(View.GONE);
                    Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                }
                break;

            case Constants.REQUEST_GET_CONFIG:
                Utils.cancelLoadingScreen();
                if (response != null) {
                    try {
                        JSONObject locObj = (JSONObject) response;
                        if (locObj.getString("result").equals("Success")) {
                            Log.d("locObj", locObj.toString());
                            motorStatusLayout.setVisibility(View.GONE);
                            layoutMotorDetails.setVisibility(View.GONE);
                            waterLevelLayout.setVisibility(View.GONE);
                            sysInfoLayout.setVisibility(View.GONE);
                            getConfigLayout.setVisibility(View.VISIBLE);
                            resultTxt.setText(locObj.getString("result"));
                            maximumMotorDuration.setText(locObj.getJSONObject("result_str").getString("Maximum Motor Duration (mins)"));
                            operationMode.setText(locObj.getJSONObject("result_str").getString("Operation Mode"));
                            dryRunProtectionEnabled.setText(locObj.getJSONObject("result_str").getString("Dry Run Protection Enabled"));
                            waitBeforeAutoRestart.setText(locObj.getJSONObject("result_str").getString("Wait Before Auto Restart (mins)"));
                        } else {
                            Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    motorStatusLayout.setVisibility(View.GONE);
                    layoutMotorDetails.setVisibility(View.GONE);
                    waterLevelLayout.setVisibility(View.GONE);
                    Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
